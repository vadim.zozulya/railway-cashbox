﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using RailwayCashbox.Data.Contexts;
using RailwayCashbox.Data.Entities;

namespace RailwayCashbox.Web.Controllers
{
	public class TrainRoutesController : Controller
	{
		private readonly Context db = new Context();

		// GET: TrainRoutes
		public ActionResult Index()
		{
			return View(db.TrainRoutes.ToList());
		}

		// GET: TrainRoutes/Details/5
		public ActionResult Details(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var trainRoute = db.TrainRoutes.Find(id);
			if (trainRoute == null)
			{
				return HttpNotFound();
			}
			return View(trainRoute);
		}

		// GET: TrainRoutes/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: TrainRoutes/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "Id,Code,StartDate,StopDate,ScheduleType")] TrainRoute trainRoute)
		{
			if (ModelState.IsValid)
			{
				db.TrainRoutes.Add(trainRoute);
				db.SaveChanges();
				return RedirectToAction("Index");
			}

			return View(trainRoute);
		}

		// GET: TrainRoutes/Edit/5
		public ActionResult Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var trainRoute = db.TrainRoutes.Find(id);
			if (trainRoute == null)
			{
				return HttpNotFound();
			}
			return View(trainRoute);
		}

		// POST: TrainRoutes/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "Id,Code,StartDate,StopDate,ScheduleType")] TrainRoute trainRoute)
		{
			if (ModelState.IsValid)
			{
				db.Entry(trainRoute).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			return View(trainRoute);
		}

		// GET: TrainRoutes/Delete/5
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			var trainRoute = db.TrainRoutes.Find(id);
			if (trainRoute == null)
			{
				return HttpNotFound();
			}
			return View(trainRoute);
		}

		// POST: TrainRoutes/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			var trainRoute = db.TrainRoutes.Find(id);
			db.TrainRoutes.Remove(trainRoute);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}