﻿using Microsoft.Owin;
using Owin;
using Startup = RailwayCashbox.Web.Startup;

[assembly: OwinStartup(typeof (Startup))]

namespace RailwayCashbox.Web
{
	public partial class Startup
	{
		public void Configuration(IAppBuilder app)
		{
			ConfigureAuth(app);
		}
	}
}