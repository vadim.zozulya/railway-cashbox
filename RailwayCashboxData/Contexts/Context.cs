﻿using System.Data.Entity;
using RailwayCashbox.Data.Entities;

namespace RailwayCashbox.Data.Contexts
{
    public class Context : DbContext
    {
        public Context() : base("MyContext")
        {
            
        }

        public DbSet<TrainRoute> TrainRoutes { get; set; }

        public DbSet<Station> Stations { get; set; }
    }
}
