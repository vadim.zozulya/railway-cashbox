﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using RailwayCashbox.Data.Entities;

namespace RailwayCashbox.Data.Contexts
{
    public class ContextInitializator : DropCreateDatabaseIfModelChanges<Context>
    {
        protected override void Seed(Context context)
        {
            var route = CreateRoute();
            context.TrainRoutes.Add(route);
            context.SaveChanges();
        }

        public static TrainRoute CreateRoute()
        {
            var kiyv = new Station { Name = "Kiyv" };
            var kharkov = new Station { Name = "Kharkov" };
            var poltava = new Station { Name = "Poltava" };

            var startTime = new TimeSpan(2, 30, 0);
            var intermediateStationArriveTime = new TimeSpan( 4, 0, 0);
            var intermediateStationDepartureTime = intermediateStationArriveTime.Add(TimeSpan.FromMinutes(20));
            var endTime = new TimeSpan( 7, 45, 0);

            var startStation = new StationStop { Station = kiyv, DepartureTime = startTime };
            var intermediateStation = new StationStop { Station = poltava, ArrivalTime = intermediateStationArriveTime, DepartureTime = intermediateStationDepartureTime };
            var endStation = new StationStop { Station = kharkov, ArrivalTime = endTime };

            var trainCars = new List<TrainCar>
            {
                new TrainCar {Number = 1, CarType = CarType.SoftSleeper},
                new TrainCar {Number = 2, CarType = CarType.SoftSleeper},
                new TrainCar {Number = 3, CarType = CarType.SoftSleeper},
                new TrainCar {Number = 4, CarType = CarType.HardSleeper},
                new TrainCar {Number = 5, CarType = CarType.HardSleeper},
                new TrainCar {Number = 6, CarType = CarType.HardSleeper},
                new TrainCar {Number = 10, CarType = CarType.HardSleeper},
                new TrainCar {Number = 12, CarType = CarType.HardSleeper},
                new TrainCar {Number = 13, CarType = CarType.HardSleeper},
            };

            var route = new TrainRoute
            {
                Code = "63A",
                StartStation = startStation,
                EndStation = endStation,
                IntermediateStations = new List<StationStop> { intermediateStation },
                TrainCars = trainCars,
                StartDate = new DateTime(DateTime.Today.Year, 2, 1),
                ScheduleType = ScheduleType.Each2Day
            };

            return route;
        }
    }
}
