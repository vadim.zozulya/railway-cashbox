using System.Collections.Generic;

namespace RailwayCashbox.Data.Entities
{
	public class Order
	{
		public Order()
		{
			//UniqueId = Guid.NewGuid();
			SeatBookings = new List<SeatBooking>();
		}

		public int Id { get; set; }
		//public Guid UniqueId { get; set; }
		public List<SeatBooking> SeatBookings { get; set; }
		public decimal TotalPrice { get; set; }
		public User User { get; set; }
	}
}