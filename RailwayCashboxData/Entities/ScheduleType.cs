namespace RailwayCashbox.Data.Entities
{
	public enum ScheduleType
	{
		Undefined = 0,
		Each1Day = 1,
		Each2Day = 2,
		Each3Day = 3,
		Each4Day = 4,
		Each5Day = 5,
		Each6Day = 6,
		Each7Day = 7
	}
}