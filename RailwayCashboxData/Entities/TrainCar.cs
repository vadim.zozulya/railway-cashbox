using RailwayCashbox.Data.Helpers;

namespace RailwayCashbox.Data.Entities
{
	public class TrainCar
	{
		public int Id { get; set; }
		public int Number { get; set; }
		public CarType CarType { get; set; }
		public decimal SeatPrice { get; set; }

		public int GetNumberOfSeats()
		{
			return NumberOfSeatsUtils.GetStandartNumberOfSeats(CarType);
		}
	}
}