namespace RailwayCashbox.Data.Entities
{
	public enum CarType
	{
		Undefined = 0,
		SoftSeat = 1, //only for seating
		HardSleeper = 2, //6 sleeping places per coupe
		SoftSleeper = 3, //4 sleeping places per coupe
		SoftSleeperDeluxe = 4 //2 sleeping places per coupe
	}
}