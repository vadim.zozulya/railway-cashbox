namespace RailwayCashbox.Data.Entities
{
	public class SeatBooking
	{
		public int Id { get; set; }
		public TrainCar TrainCar { get; set; }
		public int SeatNumber { get; set; }
		public bool Booked { get; set; }
	}
}