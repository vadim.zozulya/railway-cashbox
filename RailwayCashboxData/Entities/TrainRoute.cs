﻿using System;
using System.Collections.Generic;

namespace RailwayCashbox.Data.Entities
{
	public class TrainRoute
	{
		public TrainRoute()
		{
			IntermediateStations = new List<StationStop>();
			TrainCars = new List<TrainCar>();
		}

		public int Id { get; set; }
		public string Code { get; set; }
		public StationStop StartStation { get; set; }
		public StationStop EndStation { get; set; }
		public List<StationStop> IntermediateStations { get; set; }
		public List<TrainCar> TrainCars { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime? StopDate { get; set; }
		public ScheduleType ScheduleType { get; set; }

		public List<StationStop> GetRoute()
		{
			var result = new List<StationStop>();
			result.Add(StartStation);
			result.AddRange(IntermediateStations);
			result.Add(EndStation);
			return result;
		}
	}

	//Sample usages
}