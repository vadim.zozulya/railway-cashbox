namespace RailwayCashbox.Data.Entities
{
	public class Station
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}