using System;

namespace RailwayCashbox.Data.Entities
{
	public class StationStop
	{
		public int Id { get; set; }
		public Station Station { get; set; }
		public TimeSpan? ArrivalTime { get; set; }
		public TimeSpan? DepartureTime { get; set; }

		public TimeSpan GetStopTimeSpan()
		{
			if (!ArrivalTime.HasValue || !DepartureTime.HasValue)
				return new TimeSpan(0);

			return DepartureTime.Value - ArrivalTime.Value;
		}
	}
}