using System;
using System.Collections.Generic;

namespace RailwayCashbox.Data.Entities
{
	public class TrainScheduleRecord
	{
		public int Id { get; set; }
		public TrainRoute TrainRoute { get; set; }
		public DateTime DepartureDate { get; set; }
		public DateTime ArrivalDate { get; set; }
		public List<SeatBooking> SeatBookings { get; set; }
	}
}