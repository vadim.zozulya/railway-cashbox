using System;
using System.Collections.Generic;
using RailwayCashbox.Data.Entities;

namespace RailwayCashbox.Data.Repositories
{
	public static class TrainScheduleRepository
	{
		public static List<TrainScheduleRecord> CreateTrainScheduleRecords(
			TrainRoute trainRoute, DateTime fromDate, DateTime toDate)
		{
			if (trainRoute.StopDate.HasValue && trainRoute.StopDate.Value < fromDate)
				return new List<TrainScheduleRecord>();

			if (trainRoute.StartDate > toDate)
				return new List<TrainScheduleRecord>();

			if (fromDate > toDate)
				return new List<TrainScheduleRecord>();

			var departureDates = new List<DateTime>();
			var date = CalculateDate(fromDate, trainRoute.ScheduleType);
			while (date.HasValue && date.Value < toDate && !departureDates.Contains(date.Value))
			{
				departureDates.Add(date.Value);
				date = CalculateDate(date.Value, trainRoute.ScheduleType);
			}

			var records = new List<TrainScheduleRecord>();
			foreach (var departureDate in departureDates)
			{
				records.Add(new TrainScheduleRecord
				{
					TrainRoute = trainRoute,
					//TODO check that trainRoute.StartStation.DepartureTime and trainRoute.EndStation.ArrivalTime can be null
					DepartureDate = departureDate.Add(trainRoute.StartStation.DepartureTime.GetValueOrDefault()),
					ArrivalDate = departureDate.Add(trainRoute.EndStation.ArrivalTime.GetValueOrDefault()),
					SeatBookings = GenerateBlankSeatBookings(trainRoute)
				});
			}

			return records;
		}

		private static List<SeatBooking> GenerateBlankSeatBookings(TrainRoute trainRoute)
		{
			var result = new List<SeatBooking>();
			foreach (var trainCar in trainRoute.TrainCars)
			{
				var numberOfSeats = trainCar.GetNumberOfSeats();
				for (var i = 1; i <= numberOfSeats; i++)
				{
					result.Add(new SeatBooking {SeatNumber = i, Booked = false, TrainCar = trainCar});
				}
			}
			return result;
		}

		public static DateTime? CalculateDate(DateTime baseDateTime, ScheduleType scheduleType)
		{
			DateTime? result = null;
			switch (scheduleType)
			{
				case ScheduleType.Undefined:
					break;
				case ScheduleType.Each1Day:
					result = baseDateTime.AddDays(1);
					break;
				case ScheduleType.Each2Day:
					result = baseDateTime.AddDays(2);
					break;
				case ScheduleType.Each3Day:
					result = baseDateTime.AddDays(3);
					break;
				case ScheduleType.Each4Day:
					result = baseDateTime.AddDays(4);
					break;
				case ScheduleType.Each5Day:
					result = baseDateTime.AddDays(5);
					break;
				case ScheduleType.Each6Day:
					result = baseDateTime.AddDays(6);
					break;
				case ScheduleType.Each7Day:
					result = baseDateTime.AddDays(7);
					break;
				default:
					throw new ArgumentOutOfRangeException("scheduleType", scheduleType, null);
			}
			return result;
		}
	}
}