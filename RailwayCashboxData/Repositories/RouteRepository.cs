using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using RailwayCashbox.Data.Contexts;
using RailwayCashbox.Data.Entities;

namespace RailwayCashbox.Data.Repositories
{
	public static class RouteRepository
	{
		public static IList<TrainRoute> GetRoutes()
		{
			using (var t = new Context())
			{
				var routes = t.TrainRoutes.Include(x => x.StartStation)
					.Include(x => x.StartStation.Station)
					.AsNoTracking()
					.ToList();
				return routes;
			}
		}

		public static TrainRoute ReverseRoute(TrainRoute sampleRoute, TimeSpan startTime, string code, DateTime startDate)
		{
			var initialStartDate = sampleRoute.StartStation.DepartureTime.GetValueOrDefault();

			var startStation = new StationStop {Station = sampleRoute.EndStation.Station, DepartureTime = startTime};
			var endStation = new StationStop
			{
				Station = sampleRoute.StartStation.Station,
				ArrivalTime = startTime.Add(sampleRoute.EndStation.ArrivalTime.GetValueOrDefault() - initialStartDate)
			};

			var intermediateStations = new List<StationStop>();
			foreach (var intermediateStation in sampleRoute.IntermediateStations)
			{
				var arriveTime = startTime.Add(intermediateStation.ArrivalTime.GetValueOrDefault() - initialStartDate);
				intermediateStations.Add(new StationStop
				{
					ArrivalTime = arriveTime,
					DepartureTime =
						arriveTime.Add(intermediateStation.DepartureTime.GetValueOrDefault() -
						               intermediateStation.ArrivalTime.GetValueOrDefault()),
					Station = intermediateStation.Station
				});
			}

			var route = new TrainRoute
			{
				Code = code,
				StartStation = startStation,
				EndStation = endStation,
				IntermediateStations = intermediateStations,
				TrainCars = sampleRoute.TrainCars,
				StartDate = startDate,
				ScheduleType = sampleRoute.ScheduleType
			};

			return route;
		}
	}
}