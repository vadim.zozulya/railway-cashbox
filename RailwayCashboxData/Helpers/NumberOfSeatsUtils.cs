using System;
using RailwayCashbox.Data.Entities;

namespace RailwayCashbox.Data.Helpers
{
	public static class NumberOfSeatsUtils
	{
		public static int GetStandartNumberOfSeats(CarType carType)
		{
			var result = 0;

			switch (carType)
			{
				case CarType.Undefined:
					break;
				case CarType.SoftSeat:
					result = 60;
					break;
				case CarType.HardSleeper:
					result = 48;
					break;
				case CarType.SoftSleeper:
					result = 32;
					break;
				case CarType.SoftSleeperDeluxe:
					result = 16;
					break;
				default:
					throw new ArgumentOutOfRangeException("carType", carType, null);
			}

			return result;
		}
	}
}